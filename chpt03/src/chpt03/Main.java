package chpt03;
import java.util.Scanner;
class Person{
	private String name;
	private boolean gender;
	private int age;
	private int id;
public  Person(){
	System.out.println("This is constructor");
	System.out.printf("%s,%d,%s,%d\n",name,age,gender,id);
}
public Person(String name,int age){
	this.name=name;
	this.gender=gender;
	this.age=age;
}
public void setName(String name){
	this.name=name;
}
public String getName(){
	return name;
}
public boolean setGender(){
	this.gender=gender;
	return gender;
}
public boolean getGender(){
	return gender;
}
public int setAge(){
	this.age=age;
	return age;
}
public int getAge(){
	return age;
}
public int setId(){
	this.id=id;
	return id;
}
public int getId(){
	return id;
}
public String toString(){
	return String.format("Person [name=%s, age=%d, gender=%s, id=%d]%n",name,age,gender,id);
}
public Person(String name,int age,boolean gender){
	this(name,age);
	this.gender=gender;
}
}
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Scanner in=new Scanner(System.in);
       int n=Integer.parseInt(in.nextLine());
       Person[]persons=new Person[n];
       for(int j=0;j<persons.length;j++){
    	   Person person=new Person(in.next(),in.nextInt(),in.nextBoolean());
    	   persons[j]=person;
       }
       for(int i=persons.length-1;i>=0;i--){
    	   System.out.print(persons[i]);
       }
       
       System.out.print(new Person());
	}

}
